from django.test import TestCase, Client
from django.urls import resolve
# from findDrinks.views import findDrinks, recommendation
# from findDrinks.models import UserRequest
# from findDrinks.forms import UserRequestForm
from register.views import register
from register.models import Register_Data
from register.forms import Register_Form
from django.http import HttpRequest

# Create your tests here.
class RegisterUnitTest(TestCase):

	@classmethod
	def setUpTestData(cls):
		Register_Data.objects.create(name="Rifqi", username="Rifqi.test", email="rifqi.test@gmail.com", password="test123")


	# Test urls and views

	def test_register_url_is_exist(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)

	def test_register_using_register_func(self):
		found = resolve('/register/')
		self.assertEqual(found.func, register)

	# Test models

	def test_if_models_in_database(self):
		registerData = Register_Data.objects.create(name="Rifqi2", username="Rifqi.test2", email="rifqi.test2@gmail.com", password="test1232")
		count_registerData = Register_Data.objects.all().count()
		self.assertEqual(count_registerData, 2)

	def test_if_registerData_name_is_exist(self):
		registerDataObj = Register_Data.objects.get(id=1)
		label = registerDataObj._meta.get_field('name').verbose_name
		self.assertEqual(label, 'name')

	def test_if_registerData_username_is_exist(self):
		registerDataObj = Register_Data.objects.get(id=1)
		label = registerDataObj._meta.get_field('username').verbose_name
		self.assertEqual(label, 'username')

	def test_if_registerData_email_is_exist(self):
		registerDataObj = Register_Data.objects.get(id=1)
		label = registerDataObj._meta.get_field('email').verbose_name
		self.assertEqual(label, 'email')

	def test_if_registerData_password_is_exist(self):
		registerDataObj = Register_Data.objects.get(id=1)
		label = registerDataObj._meta.get_field('password').verbose_name
		self.assertEqual(label, 'password')

	# Test form

	def test_form_input_html(self):
		form = Register_Form()
		self.assertIn('id="id_name', form.as_p())
		self.assertIn('id="id_username', form.as_p())
		self.assertIn('id="id_email', form.as_p())
		self.assertIn('id="id_password', form.as_p())

	def test_form_validation_blank(self):
		form = Register_Form(data={'name':'', 'username':'', 'email':'', 'password':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['name'], ["This field is required."])
		self.assertEquals(form.errors['username'], ["This field is required."])
		self.assertEquals(form.errors['email'], ["This field is required."])
		self.assertEquals(form.errors['password'], ["This field is required."])

	# Test HTML

	def test_register_page_using_register_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'register.html')

	def test_landing_page_register_is_completed(self):
		request = HttpRequest()
		response = register(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Start your journey with us!', html_response)

