from django.conf import settings
from django.db import models

# Create your models here.
class Register_Data(models.Model):
	name = models.CharField(max_length=40)
	username = models.CharField(max_length=20, unique=True, error_messages={'unique':"This username is already exists"})
	email = models.EmailField(unique=True, error_messages={'unique':"This email is already exists"})
	password = models.CharField(max_length=32)