from django import forms
from .models import Register_Data


class Register_Form(forms.ModelForm):
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    email = forms.EmailField(widget=forms.EmailInput)

    class Meta:
    	model = Register_Data
    	fields = ('name', 'username', 'email', 'password')