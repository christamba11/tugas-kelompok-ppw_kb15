from django.shortcuts import render, redirect
from .models import Register_Data
from .forms import Register_Form

# Create your views here.

def register(request):
    if request.method == "POST":
        form = Register_Form(request.POST)
        if form.is_valid():
            jadwal = form.save()
            jadwal.save()        
            return redirect('login:login')
    else:
        form = Register_Form()
        
    return render(request, 'register.html', {'form': form})