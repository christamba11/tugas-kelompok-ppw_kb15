from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from .models import Login
from . import forms
from register.models import Register_Data

# Create your views here.

def login_view(request):
    form = forms.LoginForm()
    user=0
    if request.method == "POST":
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            if (request.POST['username']=='admin' and request.POST['password']=='admin'):
                return redirect('addDrink:add_drink')
            else:
                user = Register_Data.objects.filter(
                    username=request.POST['username'],
                    password= request.POST['password'],
                )
                if user.count()<=0:
                    user = None
                    return render(request, 'login/login.html', {'user':user, 'form':form})
                return redirect('findDrinks:findDrinks')
    return render(request, 'login/login.html', {'user':user, 'form':form})
            


