from django.conf.urls import url
from django.contrib import admin
from django.urls import include,path
from . import views

app_name = "login"

urlpatterns = [
    path('', views.login_view, name= 'login'),
    #url(r'login_success/$', views.login_success, name='login_success')
]