[![pipeline status](https://gitlab.com/christamba11/tugas-kelompok-ppw_kb15/badges/master/pipeline.svg)](https://gitlab.com/christamba11/tugas-kelompok-ppw_kb15/commits/master)
[![coverage report](https://gitlab.com/christamba11/tugas-kelompok-ppw_kb15/badges/master/coverage.svg)](https://gitlab.com/christamba11/tugas-kelompok-ppw_kb15/commits/master)

# Tugas Kelompok PPW
## Kelompok KB15
- 1806235681 - Christopher Yohanes Hasian Tamba
- 1806235694 - Syauqi Muhammad Dhiya Ulhaq
- 1806205685 - Exacta Febrinanto Abdillah
- 1806205621 - Muhammad Rifqi
- 1806205653 - Michael Susanto 

## Link Herokuapp
```go
https://tk-ppw15.herokuapp.com
```

## Latar Belakang Aplikasi
Ketika berkunjung ke tempat baru yang tidak dikenal, mencari kebutuhan pokok tidaklah mudah. Aplikasi Drink Finder ini merupakan sebuah aplikasi cerdas yang akan mengetahui apabila Anda sedang haus melalui suatu sistem cerdas. Sistem cerdas yang kami gunakan adalah voice recognition. Aplikasi ini tidak hanya merekomendasikan minuman apa yang akan dijual di dekat Anda, namun juga memberi informasi mengenai di mana minuman tersebut di jual di dekat lokasi Anda. Dengan adanya aplikasi ini, diharapkan dapat mempermudah kehidupan sehari-hari Anda dalam mencari minuman, terutama apabila Anda mengunjungi tempat yang belum pernah Anda kunjungi sebelumnya.

## Fitur-Fitur Yang Akan Diimplementasikan
Fitur-fitur yang akan diimplementasikan dalam app ini terdiri dari 5 fitur:
- Login (Syauqi),
User harus melakukan login terlebih dahulu untuk memanfaatkan kemudahan aplikasi ini secara maksimal.
*https://tk-ppw15.herokuapp.com*

- Register (Rifqi),
Jika user belum memiliki akun di Drink Finder, maka user dapat membuatnya terlebih dahulu.
*https://tk-ppw15.herokuapp.com/register*

- Find Drinks (Michael -> lihat contoh masukkan input untuk mendapatkan simulasi hasil rekomendasi),
User dapat menerima rekomendasi dari Drink Finder mengenai minuman-minuman yang ada di sekitar lokasi user.
*https://tk-ppw15.herokuapp.com/findDrinks*
*https://tk-ppw15.herokuapp.com/recommendation*
-> Masukkan category:milk, flavor:none, sugarRate:0%, hotOrIce=hot, blendOrBrew=blend, userLocation=Jakarta 
-> Untuk mendapatkan contoh tampilan recommendation yang benar

- Feedbacks (Exacta),
User dapat mengisi feedback demi membangun aplikasi ini.

- Admin (Christopher),
Admin dapat menambahkan data-data minuman ke database dan melihat feedbacks dari user.
*https://tk-ppw15.herokuapp.com/add_drink*
*https://tk-ppw15.herokuapp.com/delete_drink*