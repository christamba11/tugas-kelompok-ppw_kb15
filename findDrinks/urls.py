from django.conf.urls import url
from django.contrib import admin
from django.urls import include,path
from findDrinks import views

app_name = "findDrinks"

urlpatterns = [
    path('findDrinks/', views.findDrinks, name='findDrinks'),
    path('recommendation/', views.recommendation, name='recommendation'),
    path('logout/', views.logout, name='logout'),
]