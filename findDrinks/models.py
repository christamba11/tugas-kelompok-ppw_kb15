from django.db import models

# Create your models here.
class UserRequest(models.Model):
	CATEGORIES = (('milk','Milk'),('tea','Tea'),('coffee','Coffee'),('softdrinks','Soft Drinks'),('others','Others'))
	FLAVORS = (('none','None'),('chocolate','Chocolate'),('vanilla','Vanilla'),('fruit','Fruit'))
	SUGAR_RATES = (('zero','0%'),('quarter','25%'),('half','50%'),('threeQuarter','75%'),('full','100%'))
	HOT_OR_ICE = (('hot','Hot'),('ice','Ice'))
	BLEND_OR_BREW = (('blend','Blend'),('brew','BREW'))

	category = models.CharField(max_length=20, choices=CATEGORIES)
	flavor = models.CharField(max_length=20, choices=FLAVORS)
	sugarRate = models.CharField(max_length=20, choices=SUGAR_RATES)
	hotOrIce = models.CharField(max_length=20, choices=HOT_OR_ICE)
	blendOrBrew = models.CharField(max_length=20, choices=BLEND_OR_BREW)
	userLocation = models.CharField(max_length=100)