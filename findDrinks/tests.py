from django.test import TestCase, Client
from django.urls import resolve
from findDrinks.views import findDrinks, recommendation, logout
from findDrinks.models import UserRequest
from findDrinks.forms import UserRequestForm
from django.http import HttpRequest

# Create your tests here.
class FindDrinksUnitTest(TestCase):

	@classmethod
	def setUpTestData(cls):
		UserRequest.objects.create(category="Coffee", flavor="Chocolate", sugarRate="100%", hotOrIce="Hot", blendOrBrew="Brew", userLocation="Jakarta")

	# Test urls and view function used

	def test_find_drinks_url_is_exist(self):
		response = Client().get('/findDrinks/')
		self.assertEqual(response.status_code, 200)

	def test_recommendation_url_is_exist(self):
		response = Client().get('/recommendation/')
		self.assertEqual(response.status_code, 302)

	def test_logout_url_is_exist(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_find_drinks_using_findDrinks_func(self):
		found = resolve('/findDrinks/')
		self.assertEqual(found.func, findDrinks)

	def test_recommendation_using_recommendation_func(self):
		found = resolve('/recommendation/')
		self.assertEqual(found.func, recommendation)

	def test_logout_using_logout_func(self):
		found = resolve('/logout/')
		self.assertEqual(found.func, logout)


	# Test models

	def test_if_models_in_database(self):
		userReq = UserRequest.objects.create(category="Coffee", flavor="Chocolate", sugarRate="100%", hotOrIce="Hot", blendOrBrew="Brew", userLocation="Jakarta")
		countUserReq = UserRequest.objects.all().count()
		self.assertEqual(countUserReq, 2)

	def test_if_userReq_category_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('category').verbose_name
		self.assertEquals(title, 'category')

	def test_if_userReq_flavor_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('flavor').verbose_name
		self.assertEquals(title, 'flavor')

	def test_if_userReq_sugarRate_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('sugarRate').verbose_name
		self.assertEquals(title, 'sugarRate')

	def test_if_userReq_hotOrIce_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('hotOrIce').verbose_name
		self.assertEquals(title, 'hotOrIce')

	def test_if_userReq_blendOrBrew_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('blendOrBrew').verbose_name
		self.assertEquals(title, 'blendOrBrew')

	def test_if_userReq_userLocation_is_exist(self):
		userReqObj = UserRequest.objects.get(id=1)
		title = userReqObj._meta.get_field('userLocation').verbose_name
		self.assertEquals(title, 'userLocation')


	# Test form

	def test_form_input_html(self):
		form = UserRequestForm()
		self.assertIn('id="id_category', form.as_p())
		self.assertIn('id="id_flavor', form.as_p())
		self.assertIn('id="id_sugarRate', form.as_p())
		self.assertIn('id="id_hotOrIce', form.as_p())
		self.assertIn('id="id_blendOrBrew', form.as_p())
		self.assertIn('id="id_userLocation', form.as_p())

	def test_form_validation_blank(self):
		form = UserRequestForm(data={'category':'', 'flavor':'', 'sugarRate':'', 'hotOrIce':'', 'blendOrBrew':'', 'userLocation':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['category'], ["This field is required."])
		self.assertEquals(form.errors['flavor'], ["This field is required."])
		self.assertEquals(form.errors['sugarRate'], ["This field is required."])
		self.assertEquals(form.errors['hotOrIce'], ["This field is required."])
		self.assertEquals(form.errors['blendOrBrew'], ["This field is required."])
		self.assertEquals(form.errors['userLocation'], ["This field is required."])

	# Test HTML and page contents

	def test_find_drinks_using_findDrinks_template(self):
		response = Client().get('/findDrinks/')
		self.assertTemplateUsed(response, 'findDrinks/findDrinks.html')

	def test_landing_page_findDrinks_is_completed(self):
		request = HttpRequest()
		response = findDrinks(request)
		html_response = response.content.decode('utf8')
		self.assertIn('What type of drinks do you want?', html_response)

	# def test_recommendation_using_recommendation_template(self):
	# 	response = Client().get('/recommendation/')
	# 	self.assertTemplateUsed(response, 'findDrinks/recommendation.html')

	# def test_landing_page_recommendation_is_completed(self):
	# 	request = HttpRequest()
	# 	response = recommendation(request)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('Recommendations', html_response)