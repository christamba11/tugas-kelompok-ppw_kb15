from django.shortcuts import render, redirect
from findDrinks.forms import UserRequestForm
from findDrinks.models import UserRequest
from addDrink.models import Minuman

# Create your views here.
def findDrinks(request):
	response = {'form':UserRequestForm}
	return render(request, 'findDrinks/findDrinks.html', response)

def recommendation(request):
	form = UserRequestForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response = {}
		response['category'] = request.POST['category']
		response['flavor'] = request.POST['flavor']
		response['sugarRate'] = request.POST['sugarRate']
		response['hotOrIce'] = request.POST['hotOrIce']
		response['blendOrBrew'] = request.POST['blendOrBrew']
		response['userLocation'] = request.POST['userLocation']
		userReq = UserRequest(category=response['category'],
			flavor=response['flavor'],
			sugarRate=response['sugarRate'],
			hotOrIce=response['hotOrIce'],
			blendOrBrew=response['blendOrBrew'],
			userLocation=response['userLocation'])
		userReq.save()

		userReqs = Minuman.objects.filter(category=response['category'],
			flavor=response['flavor'],
			sugar_rate=response['sugarRate'],
			hot_or_ice=response['hotOrIce'],
			blend_or_brew=response['blendOrBrew'],
			store_location=response['userLocation'])
		if(userReqs.count() <= 0):
			userReqs = None
			
		response = {'userReqs' : userReqs}
		return render(request, 'findDrinks/recommendation.html', response)
	return redirect('/findDrinks/')

def logout(request):
	return redirect('login:login')