from django import forms
from findDrinks.models import UserRequest

class UserRequestForm(forms.ModelForm):
	category = forms.ChoiceField(label='Category ', choices=UserRequest.CATEGORIES)
	flavor = forms.ChoiceField(label='Flavor ', choices=UserRequest.FLAVORS)
	sugarRate = forms.ChoiceField(label='Sugar Rate ', choices=UserRequest.SUGAR_RATES)
	hotOrIce = forms.ChoiceField(label='Hot or Ice? ', choices=UserRequest.HOT_OR_ICE)
	blendOrBrew = forms.ChoiceField(label='Blend or Brew? ', choices=UserRequest.BLEND_OR_BREW)
	userLocation = forms.TextInput(attrs={'placeholder' : 'Your Location'})
	field_order = ['category', 'flavor', 'sugarRate', 'hotOrIce', 'blendOrBrew', 'userLocation']

	class Meta:
		model = UserRequest
		fields = {'category', 'flavor', 'sugarRate', 'hotOrIce', 'blendOrBrew', 'userLocation'}
		labels = {'userLocation' : 'Your Location '}