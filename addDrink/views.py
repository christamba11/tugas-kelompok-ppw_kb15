from django.shortcuts import render, redirect
from .models import Minuman
from . import forms

'''Function for handling add drink feeature'''
def add_drink(request):
    form = forms.FormAddDrink()
    if request.method == "POST":
        form = forms.FormAddDrink(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('addDrink:list_drink')
    return render(request, "addDrink/form_addDrink.html", {'form':form})

'''Outputing all drinks'''
def list_drink(request):
    list_item = Minuman.objects.order_by('drink_name')
    response = {
        'list_item': list_item,
    }
    return render(request, "addDrink/list_drink.html", response)

'''Views for deleting item'''
def delete_item(request):
    if request.method=="POST":
        id = request.POST['id']
        Minuman.objects.get(id = id).delete()
    return redirect('addDrink:list_drink')


