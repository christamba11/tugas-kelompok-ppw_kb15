from django.conf.urls import url
from django.urls import path
from . import views

app_name = "addDrink"

urlpatterns = [
    path('', views.list_drink, name="list_drink"),
    path('add_drink/', views.add_drink, name="add_drink"),
    path('delete_drink/', views.delete_item, name="delete_drink"),
]