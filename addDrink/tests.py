from django.test import TestCase, Client
from django.urls import resolve
from .views import add_drink
from .models import Minuman
from .forms import FormAddDrink
from django.http import HttpRequest

class AddDrinkUnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Minuman.objects.create(
            drink_name="Teh Tawar", category="Tea", flavor="None", sugar_rate ="0%",
            hot_or_ice="Hot", blend_or_brew = "BREW", store_name="Warung Bu Siti", 
            store_location="Jakarta", price=15000
        )

    #Test urls and view function that was being used
    # def test_add_drink_url_is_exist(self):
    #     response = Client().get('/add_drink/')
    #     self.assertEqual(response.status_code, 200)
    
    # def test_add_drink_using_add_drink_func(self):
    #     found = resolve('/add_drink/')
    #     self.assertEqual(found.func, add_drink)

    #Test models
    def test_if_models_in_database(self):
        addDrink = Minuman.objects.create(
            drink_name="Teh Tawar", category="Tea", flavor="None", sugar_rate ="0%",
            hot_or_ice="Hot", blend_or_brew = "BREW", store_name="Warung Bu Siti", 
            store_location="Jakarta", price=15000
        )
        countMinuman = Minuman.objects.all().count()
        self.assertEqual(countMinuman, 2)
    
    # def test_if_addDrink_drink_name_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title= obj._meta.get_field('drink_name').verbose_name
    #     self.assertEquals(title,'drink_name')
    
    def test_if_addDrink_category_is_exist(self):
        obj = Minuman.objects.get(id=1)
        title = obj._meta.get_field('category').verbose_name
        self.assertEquals(title, 'category')
    
    def test_if_addDrink_flavor_is_exist(self):
        obj = Minuman.objects.get(id=1)
        title = obj._meta.get_field('flavor').verbose_name
        self.assertEquals(title, 'flavor')
    
    # def test_if_addDrink_sugar_rate_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title = obj._meta.get_field('sugar_rate').verbose_name
    #     self.assertEquals(title,'sugar_rate')

    # def test_if_addDrink_hot_or_ice_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title = obj._meta.get_field('hot_or_ice').verbose_name
    #     self.assertEquals(title,'hot_or_ice')

    # def test_if_addDrink_blend_or_brew_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title = obj._meta.get_field('blend_or_brew').verbose_name
    #     self.assertEquals(title,'blend_or_brew')

    # def test_if_addDrink_store_name_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title = obj._meta.get_field('store_name').verbose_name
    #     self.assertEquals(title,'store_name')

    # def test_if_addDrink_store_location_is_exist(self):
    #     obj = Minuman.objects.get(id=1)
    #     title = obj._meta.get_field('store_location').verbose_name
    #     self.assertEquals(title,'store_location')
    
    def test_if_addDrink_price_is_exist(self):
        obj = Minuman.objects.get(id=1)
        title = obj._meta.get_field('price').verbose_name
        self.assertEquals(title,'price')

    #test Forms
	# def test_form_input_html(self):
    #     form = FormAddDrink()
    #     self.assertIn('id="id_drink_name', form.as_p())
    #     self.assertIn('id="id_category', form.as_p())
    #     self.assertIn('id="id_flavor', form.as_p())
    #     self.assertIn('id="id_sugar_rate', form.as_p())
    #     self.assertIn('id="id_hot_or_ice', form.as_p())
    #     self.assertIn('id="id_blend_or_brew', form.as_p())
    #     self.assertIn('id="id_store_name', form.as_p())
    #     self.assertIn('id="id_store_location', form.as_p())
    #     self.assertIn('id="id_price', form.as_p())

	# def test_form_validation_blank(self):
	# 	form = FormAddDrink(
    #         data={
    #             'drink_name':'', 'category':'', 'flavor':'', 'sugar_rate':'', 
    #             'hot_or_ice':'', 'blend_or_brew':'',
    #             'store_name':'', 'store_location':'', 'price':'',
    #         }
    #     )
	# 	self.assertFalse(form.is_valid())
    #     self.assertEquals(form.errors['drink_name'], ["This field is required."])
	# 	self.assertEquals(form.errors['category'], ["This field is required."])
	# 	self.assertEquals(form.errors['flavor'], ["This field is required."])
	# 	self.assertEquals(form.errors['sugar_rate'], ["This field is required."])
	# 	self.assertEquals(form.errors['hot_or_ice'], ["This field is required."])
	# 	self.assertEquals(form.errors['blend_or_brew'], ["This field is required."])
    #     self.assertEquals(form.errors['store_name'], ["This field is required."])
	# 	self.assertEquals(form.errors['store_location'], ["This field is required."])
    #     self.assertEquals(form.errors['price'], ["This field is required."])


    # #Test templates
	# def test_add_drink_using_addDrink_template(self):
	# 	response = Client().get('/addDrink/')
	# 	self.assertTemplateUsed(response, 'addDrink/form_addDrink.html')

	# def test_landing_page_findDrinks_is_completed(self):
	# 	request = HttpRequest()
	# 	response = add_drink(request)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('Add Drinks', html_response)
