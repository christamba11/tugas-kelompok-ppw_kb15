from django.db import models

class Minuman(models.Model):
    CATEGORY_CHOICES =(
        ('milk','Milk'),
        ('tea','Tea'),
        ('coffee','Coffee'),
        ('softdrinks','Soft Drinks'),
        ('others','Others')
    )
    FLAVORS = (
        ('none','None'),
        ('chocolate','Chocolate'),
        ('vanilla','Vanilla'),
        ('fruit','Fruit')
    )
    SUGAR_RATES = (
        ('zero','0%'),
        ('quarter','25%'),
        ('half','50%'),
        ('threeQuarter','75%'),
        ('full','100%')
    )
    HOT_OR_ICE = (
        ('hot','Hot'),
        ('ice','Ice'),
    )
    BLEND_OR_BREW = (
        ('blend','Blend'),
        ('brew','BREW')
    )

    drink_name = models.CharField(max_length= 75)
    category = models.CharField(max_length= 20, choices=CATEGORY_CHOICES)
    flavor = models.CharField(max_length = 20, choices = FLAVORS)
    sugar_rate = models.CharField(max_length=20, choices = SUGAR_RATES)
    hot_or_ice = models.CharField(max_length = 20, choices = HOT_OR_ICE)
    blend_or_brew = models.CharField(max_length=20, choices= BLEND_OR_BREW)
    store_name = models.CharField(max_length=75)
    store_location = models.CharField(max_length=75)
    price = models.IntegerField()
    drink_image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.drink_name
