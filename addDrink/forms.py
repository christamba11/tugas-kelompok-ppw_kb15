from .models import Minuman
from django import forms

class FormAddDrink(forms.ModelForm):
    drink_name = forms.CharField(
        widget = forms.TextInput(
            attrs= {
                "class":"txt-form",
                "required": True,
                "placeholder": "Enter the name of the drink",
            }
        )
    )
    category = forms.CharField(
        widget= forms.Select(
            attrs={
                "class" : "",
            },
            choices = Minuman.CATEGORY_CHOICES, 
        )
    )
    flavor = forms.CharField(
        widget = forms.Select(
            attrs={
                "class": "",
            },
            choices = Minuman.FLAVORS,
        )
    )
    sugar_rate = forms.CharField(
        widget = forms.Select(
            attrs= {
                "class":"",
            },
            choices=Minuman.SUGAR_RATES,
        )
    )
    hot_or_ice = forms.CharField(
        widget = forms.Select(
            attrs = {
                "class":'',
            },
            choices = Minuman.HOT_OR_ICE,
        )
    )
    blend_or_brew = forms.CharField(
        widget = forms.Select(
            attrs={
                "class":'',
            },
            choices= Minuman.BLEND_OR_BREW,
        )
    )
    store_name = forms.CharField(
        widget = forms.TextInput(
            attrs ={
                "class":'txt-form',
                "placeholder" : "Enter name of the store",
            }
        )
    )
    store_location = forms.CharField(
        widget = forms.TextInput(
            attrs ={
                "class":'txt-form',
                "placeholder" : "Enter location of the store",
            }
        )
    )
    price = forms.IntegerField(
        widget = forms.NumberInput(
            attrs = {
                "class": 'txt-form',
                "placeholder" : "Enter the price for of the drink",
            }
        )
    )
    drink_image = forms.ImageField(
        widget = forms.FileInput(
            attrs = {
                "class": '',
            }
        )
    )

    class Meta:
        model = Minuman
        fields= [
            'drink_name', 'category', 'flavor', 'sugar_rate', 'hot_or_ice', 'blend_or_brew',
            'store_name', 'store_location', 'price', 'drink_image',
        ]